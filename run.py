import sys
import json
from processor.TypeProcessor import TypeProcessor
from processor.DataProcessor import DataProcessor
import configparser
from db.mysql.MySQLHandler import MySQLHandler
import time

sys.stdout = open(1, 'w', encoding='utf-8', closefd=False) # Fix stdout-problem with german umlaute.
start_time = time.time() # Let's compute execution time.

# Read main configuration and prepare the environment.
print('Prepare environment...')
config = configparser.ConfigParser()
config.read('./conf/main.ini')
config.sections()

main_config = config['MAIN'] # Get DB-configuratioins from config-file
images_folder = main_config['images_folder']
txt_samples_folder = main_config['txt_samples_folder']
c_types = main_config['c_types'].split(',')
print('Environment is ready...\n')

for c_type in c_types:
    print("Process %s data...." % c_type)
    dp = DataProcessor()
    ep = TypeProcessor("./Data", txt_samples_folder, c_type)
    img_folder = dp.createFolderIfNotExists(images_folder)
    samples_folder = dp.createFolderIfNotExists(txt_samples_folder)
    samples_text_file = ep.getSymplesFileName()
    print("Check is %s sample file exists...." % samples_text_file)
    if ep.isFileExists(samples_text_file):
        print(samples_text_file, " exists")
    else:
        print(samples_text_file, " does not exists, create\n...")
        ep.setListOfFiles()
        ep.createTextFileWithJSONFiles()
        print(samples_text_file, " created.\n...")
    #continue
    json_files = ep.getJSONFilesFromText()
    blob_json_files = []
    main_data = []
    pressemitteilungen = []
    kontakte = []
    beschreibungen = []
    titles = []
    personalia = []
    images = []
    homepages = []
    bildmaterial = []
    marke_homepages = []
    not_processed_files = []
    
    print('--- Start JSON parsing ---')
    count = 0
    for i in json_files:
        count = count + 1 # Counting on this place because of multiple 'continue' in 'if'. Many iterations does not reach the end of the loop. 
        data = json.load(open(i))
        if data['_classname'] == 'ATFolder':
            if 'mer.hersteller.interfaces.IGrundeintragFolder' in data['_directly_provided']:
                result = dp.processMainData(data)
                main_data.append(result)
            else:
                not_processed_files.append([dp.getObjectFileAndPath(i, data)," CLASS: ",data['_classname']])    
        elif data['_classname'] == 'ATDocument':
            if 'mer.hersteller.interfaces.IPressemitteilung' in data['_directly_provided']:
                    result = dp.processPressemitteilung(data)
                    pressemitteilungen.append(result)
            elif 'mer.hersteller.interfaces.IKontakt' in data['_directly_provided']:
                    result = dp.processKontakt(data)
                    kontakte.append(result)
    
            elif 'mer.hersteller.interfaces.IGrundeintragMetaText' in data['_directly_provided']:
                    result = dp.processUnitdescription(data)
                    beschreibungen.append(result)
                    
            elif 'mer.hersteller.interfaces.IPersonalia' in data['_directly_provided']:
                    result = dp.processPersonalia(data)
                    personalia.append(result)
        elif  data['_classname'] == 'ATBlob':
            blob_json_files.append(i)
        
        elif data['_classname'] == 'Zuordnung':
            
            if 'mer.hersteller.interfaces.IWebsite' in data['_directly_provided']:
                result = dp.processHomepage(data)
                homepages.append(result)
            elif 'mer.hersteller.interfaces.IMarke' in data['_directly_provided']:
                #print(i)
                result = dp.processMarkeHomepage(data)
                marke_homepages.append(result)
            elif 'mer.hersteller.interfaces.ITitel' in data['_directly_provided']:
                #print('Titel : Not implemented because of .. nothing to process')
                result = dp.processTitel(data)
                titles.append(result)
            elif 'mer.hersteller.interfaces.ISparten' in data['_directly_provided']:
                print('Sparten are not to process: ', i)
            else:
                not_processed_files.append([dp.getObjectFileAndPath(i, data)," CLASS: ",data['_classname']])
    
        else:
            not_processed_files.append([dp.getObjectFileAndPath(i, data)," CLASS: ",data['_classname']])
 
        
print('JSON processed.. \nStart process images..')            
''' 
Images can be processed only at the end of all other objects.
This is because of filestructure creation: e.g /files/fachmedien/<first letter of event company>/<event company name>/image
Parameter <first letter of event company> and <event company name> must be extracted from full filled content type -array. 
'''
for d in blob_json_files:
    blob_data = json.load(open(d))
    if '_datafield_image' in blob_data:
        #print("Image-File: ", d)
        fm_id = dp.getUnitNameFromPath(blob_data['_path'])
        cap_letter = dp.getFirstCapLetterFromString(fm_id)
        image_path = dp.createImageFolderIfNotExists(c_type, cap_letter, fm_id)
        result = dp.processImage(blob_data, image_path)
        images.append(result)

    elif '_datafield_file' in blob_data:
        # There is no such data.
        #result = processor.processBinFile(data)
        #binfiles.append(result)
        print('Data-File found', blob_data['_path'])
        continue
    
    elif 'mer.hersteller.interfaces.IBildmaterial' in blob_data['_directly_provided']:
        result = dp.processBildmaterial(data)
        bildmaterial.append(result)
    else:
        print('Count: ', count, ' ', " : ", data['_classname'])
        print('WARNING! Not processed')
        #print(processor.getObjectFileAndPath(i, data)," CLASS: ",data['_classname'])
           

print('-------- Processing-stats---------------------------------------')       
print('JSON parsing is done.')
print('Processed: \t\t', count, " files.")
print('Not processed: \t\t', len(not_processed_files), " files.")
print("----------------------------------------------------------------")
print("~ MainData: \t\t", len(main_data))
print("~ Pressemitteilungen: \t", len(pressemitteilungen))
print("~ Kontakte: \t\t", len(kontakte))
print("~ Beschreibungen: \t", len(beschreibungen))
print("~ Personalia: \t\t", len(personalia))
print("~ Images: \t\t", len(images))
print("~ Bildmaterial: \t", len(bildmaterial))
print("~ Homepages: \t\t", len(homepages))
print("~ Marke-Homepages: \t", len(marke_homepages))
print("~ Titles: \t\t", len(titles))
print('----------------------------------------------------------------')

'''a = []
for i in images:
    image_extention = i['content_type'].split('/')[1]
    a.append(image_extention)

print(dp.getUniqueValuesList(a))  ''' 
# Put all parsed data from single arrays in event-structured data.
#--------------------------------------------------------
# Take unique event-names in a variable id.
# id is a part of path, e.g. from '/mercurio/de/fachmedien/blabla' we have 'blabla'.
# This will be an unique identifier for a single event for later identifying unique entries in other data arrays.
res = {}
print("--- Start pulling all parsed data in one single array with maintype (hersteller, events, etc.) as a key ---")
for fm in main_data:
    id = fm['path'].split('/')[-1]
    dp.createMainInstertSQL(c_type, fm)
     
   # Add appropriate images to the final maintype (hersteller, events, etc.) array.
    if len(images) > 0:
        res_img = []
        for img in images:
            if id in img['path']:
                res_img.append(img)
                dp.createImageIsertSQL(c_type, img, id)
        res['images'] = res_img
    
    # Add appropriate descriptions(firmenbeschreibungen) to final maintype (hersteller, events, etc.) array
    if len(beschreibungen) > 0:
        res_desc = []
        for desc in beschreibungen:
            if id in desc['path']:
                dp.createBeschreibungInstertSQL(c_type, desc, id)
                res_desc.append(desc)
        res['beschreibungen'] = res_desc
    
    # Add corporate-website(homepage) to final hersteller array
    if len(homepages) > 0:
        res_wbs = []
        for wbs in homepages:
            if id in wbs['path']:
                dp.createHomepageInstertSQL(c_type, wbs, id)
                res_wbs.append(wbs)
        res['homepage'] = res_wbs
    
    # Add corporate-contacts(kontakte) to final maintype (hersteller, events, etc.) array
    if len(kontakte) > 0:
        res_con = []
        for con in kontakte:
            if id in con['path']:
                dp.createKontaktInstertSQL(c_type, con, id)
                res_con.append(con)
        res['kontakte'] = res_con
    
    # Add corporate-press-releases(pressemitteilungen) to final maintype (hersteller, events, etc.) array
    if len(pressemitteilungen) > 0:
        res_press = []
        for press in pressemitteilungen:
            if id in press['path']:
                dp.createPressemitteilungInstertSQL(c_type, press, id)
                res_press.append(press)
        res['pressemitteilungen'] = res_press
    
    # Add titles to final fachmedien array
    if len(titles) > 0:
        res_titles = []
        for tit in titles:
            if id in tit['path']:
                dp.createTitleInstertSQL(c_type, tit, id)
                res_press.append(tit)
        res['titles'] = res_titles
    

    # Add personalia to final maintype (hersteller, events, etc.) array
    if len(personalia) > 0:
        res_pers = []
        for pers in personalia:
            if id in pers['path']:
                dp.createPersonaliaInstertSQL(c_type, pers, id)
                res_pers.append(pers)
        res['personalia'] = res_pers

    #continue
    # Add marke_homepages to final maintype (hersteller, events, etc.) array
    if len(personalia) > 0:
        res_mh = []
        for mh in marke_homepages:
            if id in mh['path']:
                dp.createMarkHomepageInstertSQL(c_type, mh, id)
                res_mh.append(mh)
        res['marke_homepages'] = res_mh

print("--- Start pulling all data in the database ---")
# Read configurations from config-file
config = configparser.ConfigParser()
config.read('./conf/mysql.ini')
config.sections()

db_config = config['MD-MIGRATION-DB'] # Get DB-configuratioins from config-file
mysql_handler = MySQLHandler(db_config) # Init MySQL-class with DB-params
mysql_handler.openConnection()
mysql_handler.dropTablesByContentType(c_type) # Cleanup database
mysql_handler.createTablesByContentType(c_type)
print('Start populating database...')
mysql_handler.populateDB(dp.getSQLInsertArray())
mysql_handler.closeConnection()


print("Execution Time: %s seconds" % (time.time() - start_time))
print('--- END ---')