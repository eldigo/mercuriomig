# -*- coding: utf-8 -*-
import sys
import sqlite3 as lite

'''
Main class for database operaions 
'''        
class SQLiteHandler():
    def __init__(self, dbname):
        self.dbname = dbname

    
    '''
    Oe�ffnet eine Verbindung zu der Datenbank. 
    '''    
    def openConnection(self):
        try:
            self.con = lite.connect(self.dbname)
        except lite.Error as e:
            print("Opening database error: %s" % e.args[0])
            sys.exit(1)
    '''
    Close DB connection. 
    '''  
    def closeConnection(self):
        if self.con:
            self.con.close()        
    
    '''
    Create table structure
    '''     
    def createDB(self):
        try:
            cur = self.con.cursor()
            cur.execute("CREATE TABLE IF NOT EXISTS hersteller (\
            id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, \
            herst_id TEXT NOT NULL, \
            path TEXT NOT NULL, \
            alt_title TEXT NOT NULL, \
            creation_date TEXT NOT NULL, \
            logo INTEGER, \
            description_text TEXT, \
            homepage_link_text TEXT, \
            homepage_link_url TEXT \
            );")
            self.con.commit()
        except lite.Error as e:
            print("ERROR creating the table %s:" % e.args[0])
            sys.exit(1)
        finally:
            self.closeConnection()
    
    '''
    Fügt date,time,usecase,resp_time,note ind die responsetimes-Tabelle hinzu. 
    '''         
    def insertRestTimes(self,date,time,usecase,resp_time,note):
        try:
            cur = self.con.cursor()
            cur.execute("INSERT INTO responsetimes(date,time,usecase,resp_time,note) VALUES (?,?,?,?,?)",(date,time,usecase,resp_time,note))
            self.con.commit()
        except lite.Error as e:
            print("Fehler beim schreiben der Daten in der DB: %s" % e.args[0])
            pass