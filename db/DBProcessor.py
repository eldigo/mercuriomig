# -*- coding: iso-8859-1 -*-
import datetime
from db.sqlite.SQLiteHandler import SQLiteHandler


class SQLiteProcessor():
    def __init__(self, db):
        self.db = db
        
    def appendToOut(self,usecase,response_time):
        now = datetime.datetime.now()
        datum = now.strftime("%d-%m-%Y")
        uhrzeit = now.strftime("%H:%M:%S")
        fo = open(self.file, "a")
        #self.logger.debug("Schreibe in %s:" % self.file)
        fo.write( "\n%s;%s;%s;%s" % (datum, uhrzeit, usecase,response_time))
        #self.logger.debug("Schliesse Verbindung zu %s:" % self.file)
        fo.close()
        dbhandler = SQLiteHandler(self.db,self.logger)
        dbhandler.openConnection()
        dbhandler.insertRestTimes(datum, uhrzeit, usecase,response_time,'')
        dbhandler.closeConnection()