# -*- coding: utf-8 -*-
import mysql.connector
from mysql.connector import errorcode


'''
Main class for database operaions 
'''        
class MySQLProcessor():
    def __init__(self, connection):
        self.cnx = connection
    
    def populateDB(self, arr):
        cursor = self.cnx.cursor()
        for sql in arr:
            try:
                print("Inserting: ", sql)
                cursor.execute(sql)
            except mysql.connector.Error as err:
                print('ERROR: ', err.msg)
            else:
                print("OK")

        cursor.close()
        