# -*- coding: utf-8 -*-
import mysql.connector
from mysql.connector import errorcode


'''
Main class for database operaions 
'''        
class MySQLHandler():
    def __init__(self, db_config):
        self.host = db_config['host']
        self.database = db_config['database']
        self.user = db_config['user']
        self.password = db_config['password']
        self.cnx = mysql.connector.connect()
        self.AGENTUREN = {}
        self.TABLES = {}
        self.initTablesByContentType('hersteller')
        self.initTablesByContentType('marken')
        self.initTablesByContentType('agenturen')
        self.initTablesByContentType('events')
        self.initTablesByContentType('fachmedien')
        self.initTablesByContentType('gastronomie')
        self.initTablesByContentType('verbaende')
        self.initTablesByContentType('zulieferer')
        '''
        self.TABLES['hersteller'] = (
                               "CREATE TABLE IF NOT EXISTS `hersteller` ("
                               "  `hersteller_id` VARCHAR(36),"
                               " `t3_uid` INT(11),"
                               "  `content_type` VARCHAR(16),"
                               "  `path` TEXT,"
                               "  `produktgruppe` TEXT,"
                               "  `creation_date` INT(11) NOT NULL,"
                               "  `tag_intern` TEXT,"
                               "  `alt_title` TEXT"
                               ") ENGINE=InnoDB")
        
        self.TABLES['firmenbeschreibungen'] = (
                               "CREATE TABLE IF NOT EXISTS `firmenbeschreibungen` ("
                               " `t3_uid` INT(11),"
                               " `t3_content_uid` INT(11),"
                               "  `hersteller_id` VARCHAR(36),"
                               "  `path` TEXT,"
                               "  `text` TEXT"
                               ") ENGINE=InnoDB")
        
        self.TABLES['homepages'] = (
                               "CREATE TABLE IF NOT EXISTS `homepages` ("
                               "  `hersteller_id` VARCHAR(36),"
                               "  `link_text` TEXT,"
                               "  `link_url` TEXT,"
                               "  `path` TEXT"
                               ") ENGINE=InnoDB")
        
        self.TABLES['images'] = (
                               "CREATE TABLE IF NOT EXISTS `images` ("
                               "  `hersteller_id` VARCHAR(36),"
                               "  `image_id` TEXT,"
                               "  `content_type` VARCHAR(16),"
                               "  `title` TEXT,"
                               "  `image` TEXT,"
                               "  `path` TEXT"
                               ") ENGINE=InnoDB")
        
        self.TABLES['kontakte'] = (
                               "CREATE TABLE IF NOT EXISTS `kontakte` ("
                               "  `hersteller_id` VARCHAR(36),"
                               "  `text` TEXT,"
                               "  `path` TEXT"
                               ") ENGINE=InnoDB")
        
        self.TABLES['markenhomepages'] = (
                               "CREATE TABLE IF NOT EXISTS `markenhomepages` ("
                               "  `hersteller_id` VARCHAR(36),"
                               "  `id` TEXT,"
                               "  `content_type` VARCHAR(16),"
                               "  `tag_intern` TEXT,"
                               "  `link_url` TEXT,"
                               "  `path` TEXT"
                               ") ENGINE=InnoDB")

        self.TABLES['personalia'] = (
                               "CREATE TABLE IF NOT EXISTS `personalia` ("
                               "  `hersteller_id` VARCHAR(36),"
                               "  `id` TEXT,"
                               "  `produktgruppe` TEXT,"
                               "  `tag_intern` TEXT,"
                               "  `grundeintrag` TEXT,"
                               "  `text` TEXT,"
                               "  `description` TEXT,"
                               "  `subline` TEXT,"
                               "  `teaser` TEXT,"
                               "  `path` TEXT"
                               ") ENGINE=InnoDB")
        
        self.TABLES['pressemitteilungen'] = (
                               "CREATE TABLE IF NOT EXISTS `pressemitteilungen` ("
                               "  `hersteller_id` VARCHAR(36),"
                               "  `id` TEXT,"
                               "  `produktgruppe` TEXT,"
                               "  `tag_intern` TEXT,"
                               "  `grundeintrag` TEXT,"
                               "  `text` TEXT,"
                               "  `creation_date` INT(11),"
                               "  `description` TEXT,"
                               "  `subline` TEXT,"
                               "  `teaser` TEXT,"
                               "  `path` TEXT"
                               ") ENGINE=InnoDB")
        
        self.AGENTUREN['agenturen'] = (
                               "CREATE TABLE IF NOT EXISTS `agenturen` ("
                               "  `id` VARCHAR(36),"
                               " `t3_uid` INT(11),"
                               "  `content_type` VARCHAR(16),"
                               "  `path` TEXT,"
                               "  `creation_date` INT(11),"
                               "  `tag_intern` TEXT,"
                               "  `alt_title` TEXT,"
                               "  `trends` TEXT"
                               ") ENGINE=InnoDB")
        self.AGENTUREN['agentur_images'] = (
                               "CREATE TABLE IF NOT EXISTS `agentur_images` ("
                               "  `agentur_id` VARCHAR(36),"
                               "  `image_id` TEXT,"
                               "  `content_type` VARCHAR(16),"
                               "  `title` VARCHAR(16),"
                               "  `image` VARCHAR(512),"
                               "  `path` TEXT"
                               ") ENGINE=InnoDB")
        self.AGENTUREN['agenturbeschreibungen'] = (
                               "CREATE TABLE IF NOT EXISTS `agenturbeschreibungen` ("
                               "  `agentur_id` VARCHAR(36),"
                               "  `path` TEXT,"
                               "  `text` TEXT"
                               ") ENGINE=InnoDB")
        self.AGENTUREN['agentur_homepages'] = (
                               "CREATE TABLE IF NOT EXISTS `agentur_homepages` ("
                               "  `agentur_id` VARCHAR(36),"
                               "  `link_text` TEXT,"
                               "  `link_url` TEXT,"
                               "  `path` TEXT"
                               ") ENGINE=InnoDB")
        self.AGENTUREN['agentur_kontakte'] = (
                               "CREATE TABLE IF NOT EXISTS `agentur_kontakte` ("
                               "  `agentur_id` VARCHAR(36),"
                               "  `text` TEXT,"
                               "  `path` TEXT"
                               ") ENGINE=InnoDB")
        
        self.AGENTUREN['agentur_pressemitteilungen'] = (
                               "CREATE TABLE IF NOT EXISTS `agentur_pressemitteilungen` ("
                               "  `agentur_id` VARCHAR(36),"
                               "  `id` TEXT,"
                               "  `produktgruppe` TEXT,"
                               "  `tag_intern` TEXT,"
                               "  `grundeintrag` TEXT,"
                               "  `text` TEXT,"
                               "  `creation_date` INT(11),"
                               "  `description` TEXT,"
                               "  `subline` TEXT,"
                               "  `teaser` TEXT,"
                               "  `path` TEXT"
                               ") ENGINE=InnoDB")
        self.AGENTUREN['agentur_personalia'] = (
                               "CREATE TABLE IF NOT EXISTS `agentur_personalia` ("
                               "  `agentur_id` VARCHAR(36),"
                               "  `id` TEXT,"
                               "  `produktgruppe` TEXT,"
                               "  `tag_intern` TEXT,"
                               "  `grundeintrag` TEXT,"
                               "  `text` TEXT,"
                               "  `description` TEXT,"
                               "  `subline` TEXT,"
                               "  `teaser` TEXT,"
                               "  `path` TEXT"
                               ") ENGINE=InnoDB")

'''    
    '''
    Connection opening, what else .. ?
    '''    
    def openConnection(self):
        try:
            self.cnx = mysql.connector.connect(user=self.user,
                                          password=self.password,
                                          host=self.host,
                                          database=self.database,
                                          use_pure=False)
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Something is wrong with your user name or password")
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                print("Database does not exist")
            else:
                print(err)
                self.cnx.close()
                
    '''
    Close DB connection. 
    '''  
    def closeConnection(self):
        if self.cnx:
            self.cnx.close()        
    
    def dropTables(self):
        cursor = self.cnx.cursor()
        for (name, ddl) in self.TABLES.items():
            drop_sql = "DROP TABLE `"+name+"`;"
            try:
                print("DROP table {}: ".format(name), end='')
                cursor.execute(drop_sql)
            except mysql.connector.Error as err:
                print('ERROR: ', err.msg)
                print(name, ' ', drop_sql)
            else:
                print("OK")

        cursor.close()
        #self.cnx.close()
    
    def dropAgenturenTables(self):
        cursor = self.cnx.cursor()
        for (name, ddl) in self.AGENTUREN.items():
            drop_sql = "DROP TABLE `"+name+"`;"
            try:
                print("DROP table {}: ".format(name), end='')
                cursor.execute(drop_sql)
            except mysql.connector.Error as err:
                print('ERROR: ', err.msg)
                print(name, ' ', drop_sql)
            else:
                print("OK")

        cursor.close()
        
    def dropTablesByContentType(self, content_type):
        cursor = self.cnx.cursor()
        for (name, ddl) in self.TABLES.items():
            if content_type in name:
                drop_sql = "DROP TABLE `"+name+"`;"
                try:
                    print("DROP table {}: ".format(name), end='')
                    cursor.execute(drop_sql)
                except mysql.connector.Error as err:
                    print('ERROR: ', err.msg)
                    print(name, ' ', drop_sql)
                else:
                    print("OK")
        cursor.close()
        
    '''
    Create table structure
    '''     
    def createTables(self):
        cursor = self.cnx.cursor()
        for (name, ddl) in self.TABLES.items():
            try:
                print("Creating table {}: ".format(name), end='')
                print(name, '', ddl)
                cursor.execute(ddl)
            except mysql.connector.Error as err:
                if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
                    print("already exists.")
                else:
                    print('ERROR: ', err.msg)
                    print(name, ' ', ddl)
            else:
                print("OK")

        cursor.close()
        #self.cnx.close()
        
    
    def initTablesByContentType(self, content_type):
            self.TABLES[content_type] = (
                               "CREATE TABLE IF NOT EXISTS `"+content_type+"` ("
                               "  `id` VARCHAR(36),"
                               " `t3_uid` INT(11),"
                               "  `content_type` VARCHAR(16),"
                               "  `path` TEXT,"
                               "  `produktgruppe` TEXT,"
                               "  `creation_date` INT(11) NOT NULL,"
                               "  `tag_intern` TEXT,"
                               "  `alt_title` TEXT,"
                               "  `trends` TEXT"
                               ") ENGINE=InnoDB")
        
            self.TABLES[content_type+'_firmenbeschreibungen'] = (
                                                               "CREATE TABLE IF NOT EXISTS `"+content_type+"_firmenbeschreibungen` ("
                                                               " `t3_uid` INT(11),"
                                                               " `t3_content_uid` INT(11),"
                                                               "  `id` VARCHAR(36),"
                                                               "  `path` TEXT,"
                                                               "  `text` TEXT"
                                                               ") ENGINE=InnoDB")
        
            self.TABLES[content_type+'_homepages'] = (
                                                   "CREATE TABLE IF NOT EXISTS `"+content_type+"_homepages` ("
                                                   "  `id` VARCHAR(36),"
                                                   "  `link_text` TEXT,"
                                                   "  `link_url` TEXT,"
                                                   "  `path` TEXT"
                                                   ") ENGINE=InnoDB")
            self.TABLES[content_type+'_titles'] = (
                                                   "CREATE TABLE IF NOT EXISTS `"+content_type+"_titles` ("
                                                   "  `"+content_type+"_id` VARCHAR(36),"
                                                   "  `tag_intern` TEXT,"
                                                   "  `extern` TEXT,"
                                                   "  `path` TEXT"
                                                   ") ENGINE=InnoDB")
            
            self.TABLES[content_type+'_images'] = (
                                               "CREATE TABLE IF NOT EXISTS `"+content_type+"_images` ("
                                               "  `"+content_type+"_id` VARCHAR(36),"
                                               "  `image_id` TEXT,"
                                               "  `content_type` VARCHAR(16),"
                                               "  `title` TEXT,"
                                               "  `image` TEXT,"
                                               "  `path` TEXT"
                                               ") ENGINE=InnoDB")
            
            self.TABLES[content_type+'_kontakte'] = (
                                                   "CREATE TABLE IF NOT EXISTS `"+content_type+"_kontakte` ("
                                                   "  `id` VARCHAR(36),"
                                                   "  `text` TEXT,"
                                                   "  `path` TEXT"
                                                   ") ENGINE=InnoDB")
            
            self.TABLES[content_type+'_markenhomepages'] = (
                                                       "CREATE TABLE IF NOT EXISTS `"+content_type+"_markenhomepages` ("
                                                       "  `"+content_type+"_id` VARCHAR(36),"
                                                       "  `id` TEXT,"
                                                       "  `content_type` VARCHAR(16),"
                                                       "  `tag_intern` TEXT,"
                                                       "  `link_url` TEXT,"
                                                       "  `path` TEXT"
                                                       ") ENGINE=InnoDB")
    
            self.TABLES[content_type+'_personalia'] = (
                                           "CREATE TABLE IF NOT EXISTS `"+content_type+"_personalia` ("
                                           "  `"+content_type+"_id` VARCHAR(36),"
                                           "  `id` TEXT,"
                                           "  `produktgruppe` TEXT,"
                                           "  `tag_intern` TEXT,"
                                           "  `grundeintrag` TEXT,"
                                           "  `text` TEXT,"
                                           "  `description` TEXT,"
                                           "  `subline` TEXT,"
                                           "  `teaser` TEXT,"
                                           "  `path` TEXT"
                                           ") ENGINE=InnoDB")
            
            self.TABLES[content_type+'_pressemitteilungen'] = (
                                           "CREATE TABLE IF NOT EXISTS `"+content_type+"_pressemitteilungen` ("
                                           "  `"+content_type+"_id` VARCHAR(36),"
                                           "  `id` TEXT,"
                                           "  `produktgruppe` TEXT,"
                                           "  `tag_intern` TEXT,"
                                           "  `grundeintrag` TEXT,"
                                           "  `text` TEXT,"
                                           "  `creation_date` INT(11),"
                                           "  `description` TEXT,"
                                           "  `subline` TEXT,"
                                           "  `teaser` TEXT,"
                                           "  `path` TEXT,"
                                           "  `title` TEXT"
                                           ") ENGINE=InnoDB")
            self.TABLES[content_type+'_bildmaterial'] = (
                                           "CREATE TABLE IF NOT EXISTS `"+content_type+"_bildmaterial` ("
                                           "  `"+content_type+"_id` VARCHAR(36),"
                                           "  `content_type` TEXT,"
                                           "  `id` TEXT,"
                                           "  `path` TEXT,"
                                           "  `teaser` TEXT,"
                                           "  `produktgruppe` TEXT,"
                                           "  `text` TEXT,"
                                           "  `tag_intern` TEXT,"
                                           "  `description` TEXT,"
                                           "  `subline` TEXT,"
                                           "  `creation_date` INT(11)"
                                           ") ENGINE=InnoDB")
    
    '''
    Create table structure by content type. E.g by 'events'
    '''     
    def createTablesByContentType(self, content_type):
            cursor = self.cnx.cursor()
            for (name, ddl) in self.TABLES.items():
                if content_type in name:
                    try:
                        print("Creating table {}: ".format(name), end='')
                        print(name, '', ddl)
                        cursor.execute(ddl)
                    except mysql.connector.Error as err:
                        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
                            print("already exists.")
                        else:
                            print('ERROR: ', err.msg)
                            print(name, ' ', ddl)
                    else:
                        print("OK")
            cursor.close()
            #self.cnx.close()
        
    def populateDB(self, arr):
        cursor = self.cnx.cursor()
        print("Running %s inserts.. " % len(arr))
        for i,j  in arr:
            try:
                #print("Inserting: ", i, ' ', j)
                cursor.execute(i, j)
                self.cnx.commit()
                #print(j[0], " successfully inserted")
            except mysql.connector.Error as err:
                print('ERROR: ', err.msg)
                print("Inserting: ", i, ' ', j)
        print(len(arr), " records added.")
        cursor.close()
    
    def getConnection(self):
        return self.cnx
    
    def getLastSortingInCategory(self, pid):
        query = "SELECT max(sorting) from sys_category where pid=%s;" % (pid)
        cursor = self.cnx.cursor()
        cursor.execute(query)
        data = cursor.fetchone()
        return data
    
    def getTitlesFromSysCategory(self):
        query = "SELECT title from sys_category;"
        cursor = self.cnx.cursor()
        cursor.execute(query)
        data = cursor.fetchall()
        return data
    
    def getAltTitlesFromHersteller(self):
        query = "SELECT alt_title from hersteller;"
        cursor = self.cnx.cursor()
        cursor.execute(query)
        data = cursor.fetchall()
        return data
    
    # Insert exported unique SysCategories in 'sys_category"-table in T3. 
    # pid is a ... pid.
    # 'start_sort' is maximal-sort-number+1, 
    # user_id is a cruser_id
    # so we can see all inserted data and deal with it if nesessary
    def insertCategoriesInSysCategory(self, pid, arr, start_sort, user_id):
        count = start_sort + 1
        cursor = self.cnx.cursor()
        for i in arr:
            query = "INSERT INTO `sys_category` (`pid`, `tstamp`, `crdate`, `cruser_id`, sorting, title, parent) VALUES (%s, %s, %s, %s, %s, %s, %s);"
            print(query % (pid, 1454863859, 1454863771, user_id, count, i, 1))
            try:
                cursor.execute(query, (pid, 1454863859, 1454863771, user_id, count, i, 1))
                self.cnx.commit()
                count = count + 1
            except mysql.connector.Error as err:
                print('ERROR: ', err.msg)
                print("Inserting: ", query, ' ', (pid, 1454863859, 1454863771, user_id, count, i, 1))
            
            
    def createAgenturenTables(self):
        cursor = self.cnx.cursor()
        for (name, ddl) in self.AGENTUREN.items():
            try:
                print("Creating table {}: ".format(name), end='')
                print(name, '', ddl)
                cursor.execute(ddl)
            except mysql.connector.Error as err:
                if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
                    print("already exists.")
                else:
                    print('ERROR: ', err.msg)
                    print(name, ' ', ddl)
            else:
                print("OK")

        cursor.close()
            