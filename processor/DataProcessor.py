import os
from PIL import Image
import base64
import io
import datetime
import time

class DataProcessor():
    def __init__(self):
        self.image_folder_path = './files'
        self.sql_inserts = []
        return None
        
    def processMainData(self, data):
        res = {}
        res['content_type'] = 'firmenbeschreibung'
        res['id'] = data['id']
        res['path'] = data['_path']
        res['alt_title'] = data['alt_title']
        res['produktgruppe'] = data['produktgruppe']
        res['tag_intern'] = data['tag_intern']
        res['trends'] = data['trends']
        res['creation_date'] = data['creation_date']
        print(data['produktgruppe'])
        return res   
    
    def processPressemitteilung(self, data):
        res = {}
        res['content_type'] = 'pressemitteilung'
        res['id'] = data['id']
        res['path'] = data['_path']
        res['title'] = data['title']
        res['teaser'] = data['teaser']
        res['grundeintrag'] = data['grundeintrag']
        res['produktgruppe'] = data['produktgruppe']
        res['text'] = data['text']
        res['tag_intern'] = data['tag_intern']
        res['creation_date'] = data['creation_date']
        res['description'] = data['description']
        res['subline'] = data['subline']
        return res 
    
    def processKontakt(self, data):
        res = {}
        res['content_type'] = 'kontakt'
        res['id'] = data['id']
        res['path'] = data['_path']
        res['text'] = data['text']
        return res
    
    def processPersonalia(self, data):
        res = {}
        res['content_type'] = 'personalia'
        res['id'] = data['id']
        res['path'] = data['_path']
        res['teaser'] = data['teaser']
        res['text'] = data['text']
        res['grundeintrag'] = data['grundeintrag']
        res['produktgruppe'] = data['produktgruppe']
        res['tag_intern'] = data['tag_intern']
        res['description'] = data['description']
        res['subline'] = data['subline']
        return res
    
    def processUnitdescription(self, data):
        res = {}
        res['content_type'] = 'firmenbeschreibung'
        res['id'] = data['id']
        res['path'] = data['_path']
        res['text'] = data['text']
        return res
    
    def processImage(self, data, images_path):
        res = {}
        image_name = self.createImageName(data['_path'])
        image_data = data['_datafield_image']
        res['id'] = data['id']
        res['path'] = data['_path']
        res['title'] = data['title']
        res['content_type'] = 'image'
        # Process Image
        image = self.createImage(image_name, images_path, image_data)
        res['image'] = image
        return res 
       
    def createImage(self, image_name, image_path, image_data):
        image_encoding = image_data['encoding'] # e.g base64. Not used at the moment
        image_data_string = image_data['data'] # string representation of the image
        image_extention = image_data['content_type'].split('/')[1]
        image_sizes = image_data['size'] # [x,y]
        name = image_path+'/'+image_name+'.'+image_extention
        image_data_decoded = base64.b64decode(image_data_string)
        # print("++++++ EXT: %s" % image_extention)
        if (image_extention != "octet-stream") and (image_extention != "tiff"):
            # Is this image already created and saved?
            if not os.path.exists(name):
                # print('Create-Image: %s' % name )
                try:    
                    fake_file = io.BytesIO()
                    image = Image.open(io.BytesIO(image_data_decoded))
                    image.save(fake_file, image_extention)
                    open(name, "wb").write(fake_file.getvalue())
                except e:
                    print(e)
                    return False
                
                return name
            else:
                #print("Already exists: %s" % name)
                return name
        else:
            print("Not supported extension!! %s" % name)
                
    def createImageName(self, path):
        name = path.split("/")
        name = self.createPathNameUnderscored(name[-1])
        return name        
    
    def processHomepage(self, data):
        res = {}
        res['content_type'] = 'homepage'
        res['id'] = data['id']
        res['path'] = data['_path']
        res['url'] = data['url']
        res['extern'] = data['extern']
        res['tag_intern'] = data['tag_intern']
        return res
        
    def processMarkeHomepage(self, data):
        res = {}
        res['content_type'] = 'marke_homepage'
        res['id'] = data['id']
        res['path'] = data['_path']
        res['extern'] = data['extern']
        res['tag_intern'] = data['tag_intern']
        return res
    
    def processTitel(self, data):
        res = {}
        res['content_type'] = 'titel'
        res['id'] = data['id']
        res['path'] = data['_path']
        res['tag_intern'] = data['tag_intern']
        res['extern'] = data['extern']
        return res
        
        
    def processBildmaterial(self, data):
        res = {}
        res['content_type'] = 'bildmaterial'
        res['id'] = data['id']
        res['path'] = data['_path']
        res['teaser'] = data['teaser']
        res['produktgruppe'] = data['produktgruppe']
        res['text'] = data['text']
        res['tag_intern'] = data['tag_intern']
        res['description'] = data['description']
        res['subline'] = data['subline']
        res['creation_date'] = data['creation_date']
        return res

    def getObjectFileAndPath(self, file, data):
        str = file + ": " + data['_path']
        return str
    
    def getUnitNameFromPath(self, path):
        name = path.split('/')[4]
        return name
        
    def getUnitAltNameForID(self, id, units_arr):
        for i in units_arr:
            if id == i['id']:
                #print("ID: %s Ev-ID: %s Ev-Alt-Title: %s" % (id, i['id'], i['alt_title']))
                return i['alt_title']

    def  getFirstCapLetterFromString(self, string):
        return string[0].capitalize()
    
    def createPathNameUnderscored(self, name):
        string = name.replace(' ', '_')
        string = string.replace('&', '_')
        string = string.replace('.', '_')
        return string
    
    def createImageFolderIfNotExists(self, content_type, letter, folder_name):
        path = self.image_folder_path + '/' + content_type + '/' + letter + '/' + folder_name
        #print(os.path.abspath(path))
        if not os.path.exists(path):
            os.makedirs(path)
        return path
    
    def createFolderIfNotExists(self, foldername):
        if not os.path.exists(foldername):
            os.makedirs(foldername)
        return foldername
    
    def createDateTime(self, dtm):
        g_string = dtm.split('.')
        del g_string[-1]
        #string = datetime.datetime.strptime(''.join(g_string), "%Y/%m/%d %H:%M:%S").strftime("%Y-%m-%d %H:%M:%S")
        string = datetime.datetime.strptime(''.join(g_string), "%Y/%m/%d %H:%M:%S")
        ans_time = time.mktime(string.timetuple())
        return ans_time
    
    def getSQLInsertArray(self):
        return self.sql_inserts
    
    ''' SQL Inserts'''
    def createMainInstertSQL(self, content_type, arr):
         #print(arr)
         mysql_query = []
         sql_str = "INSERT INTO "+content_type+" (id, content_type, path, creation_date, tag_intern, produktgruppe, alt_title, trends) VALUES(%s, %s, %s, %s, %s, %s, %s, %s);"
         sql_values = (arr['id'], arr['content_type'], arr['path'], self.createDateTime(arr['creation_date']), ', '.join(arr['tag_intern']), ', '.join(arr['produktgruppe']), arr['alt_title'],', '.join(arr['trends']))
         #print(sql_str % sql_values)
         mysql_query.insert(0, sql_str)
         mysql_query.insert(1, sql_values)
         self.sql_inserts.append(mysql_query)        
        
    def createImageIsertSQL(self, content_type, img, id):
        mysql_query = []
        sql_str = "INSERT INTO "+content_type+"_images ("+content_type+"_id, image_id, content_type, title, image, path) VALUES(%s,%s,%s,%s,%s,%s);"
        sql_values = (id, img['id'], img['content_type'], img['title'], img['image'], img['path'])
        #print(sql_str % sql_values)
        mysql_query.insert(0, sql_str)
        mysql_query.insert(1, sql_values)              
        self.sql_inserts.append(mysql_query)
        
    def createBeschreibungInstertSQL(self, content_type, desc, id):
        mysql_query = []
        sql_str = "INSERT INTO " +content_type+"_firmenbeschreibungen (id, text, path) VALUES(%s,%s,%s);"
        sql_values = (id, desc['text'], desc['path'])
        #print(sql_str % sql_values)
        mysql_query.insert(0, sql_str)
        mysql_query.insert(1, sql_values)
        self.sql_inserts.append(mysql_query)
        
    def createHomepageInstertSQL(self, content_type, arr, id):
        mysql_query = []
        sql_str = "INSERT INTO "+content_type+"_homepages (id, link_text, link_url, path) VALUES(%s,%s,%s, %s);"
        sql_values = (id, ''.join(arr['url']), arr['extern'], arr['path'])
        #print(sql_str % sql_values)
        mysql_query.insert(0, sql_str)
        mysql_query.insert(1, sql_values)
        self.sql_inserts.append(mysql_query)
        
    def createKontaktInstertSQL(self, content_type, arr, id):
        mysql_query = []
        sql_str = "INSERT INTO "+content_type+"_kontakte (id, text, path) VALUES(%s, %s, %s);"
        sql_values = (id, arr['text'], arr['path'])
        #print(sql_str % sql_values)
        mysql_query.insert(0, sql_str)
        mysql_query.insert(1, sql_values)
        self.sql_inserts.append(mysql_query)  
        
    def createPressemitteilungInstertSQL(self, content_type, arr, id):
        mysql_query = []
        sql_str = "INSERT INTO "+content_type+"_pressemitteilungen ("+content_type+"_id, path, produktgruppe, grundeintrag, text, creation_date, id, tag_intern, description, subline, teaser, title) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);"
        sql_values = (id, arr['path'], ','.join(arr['produktgruppe']), ','.join(arr['grundeintrag']), arr['text'], self.createDateTime(arr['creation_date']), arr['id'], ','.join(arr['tag_intern']), arr['description'], arr['subline'], arr['teaser'], arr['title'])
        #print(sql_str % sql_values)
        mysql_query.insert(0, sql_str)
        mysql_query.insert(1, sql_values)
        self.sql_inserts.append(mysql_query)        
        
    def createMarkHomepageInstertSQL(self, content_type, arr, id):
        mysql_query = []
        sql_str = "INSERT INTO "+content_type+"_markenhomepages ("+content_type+"_id, content_type, id, tag_intern, link_url, path) VALUES(%s, %s, %s, %s, %s, %s);"
        sql_values = (id, arr['content_type'], arr['id'], ', '.join(arr['tag_intern']), arr['extern'], arr['path'])
        #print(sql_str % sql_values)
        mysql_query.insert(0, sql_str)
        mysql_query.insert(1, sql_values)
        self.sql_inserts.append(mysql_query)
        
    def createTitleInstertSQL(self, content_type, arr, id):
        mysql_query = []
        sql_str = "INSERT INTO "+content_type+"_titles ("+content_type+"_id, tag_intern, extern, path) VALUES(%s, %s, %s, %s);"
        sql_values = (id, ', '.join(arr['tag_intern']), arr['extern'], arr['path'])
        #print(sql_str % sql_values)
        mysql_query.insert(0, sql_str)
        mysql_query.insert(1, sql_values)
        self.sql_inserts.append(mysql_query)   
        
    def createPersonaliaInstertSQL(self, content_type, arr, id):
         mysql_query = []
         sql_str = "INSERT INTO "+content_type+"_personalia ("+content_type+"_id, path, produktgruppe, tag_intern, grundeintrag, text, id, description, subline, teaser)  VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s);"
         sql_values = (id, arr['path'], ','.join(arr['produktgruppe']), ', '.join(arr['tag_intern']),','.join(arr['grundeintrag']), arr['text'], arr['id'], arr['description'], arr['subline'], arr['teaser'])
         #print(sql_str % sql_values)
         mysql_query.insert(0, sql_str)
         mysql_query.insert(1, sql_values)
         self.sql_inserts.append(mysql_query)
         
    def createBildmaterialInstertSQL(self, content_type, arr, id):
         mysql_query = []
         sql_str = "INSERT INTO "+content_type+"_bildmaterial ("+content_type+"_id, id, path, teaser, produktgruppe, text, tag_intern,  description, subline, creation_date)  VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s);"
         sql_values = (id, arr['id'], arr['path'], arr['teaser'], ','.join(arr['produktgruppe']), arr['text'], ', '.join(arr['tag_intern']), arr['description'], arr['subline'], self.createDateTime(arr['creation_date']))
         print(sql_str % sql_values)
         mysql_query.insert(0, sql_str)
         mysql_query.insert(1, sql_values)
         self.sql_inserts.append(mysql_query)
     
    '''TOOLS'''
    def analyzeNotProcessed(self, arr):
        for i in arr:
            print(i)        
        
    def getUniqueValuesList(self, arr):
        a = set(arr)
        return sorted(list(a))
        
        