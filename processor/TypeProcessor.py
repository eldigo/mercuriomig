# -*- coding: utf-8 -*-
from processor.JSONProcessor import JSONProcessor
import json
import os
'''
Main class for Type operaions 
'''        
class TypeProcessor():
    def __init__(self, json_folder, samples_folder, c_type):
        self.c_type = c_type
        self.text_with_json_filenames = samples_folder+'/alle-'+c_type+'.txt'
        self.classnames_to_process = [\
                                      'ATFolder',\
                                      'ATDocument',\
                                      'ATBlob',\
                                      'Zuordnung'\
                                      ]
        self.interfaces_to_process = [\
                                      'mer.hersteller.interfaces.IVeranstaltungen'
                                      ]
        self.jsp = JSONProcessor(json_folder)

    def getSymplesFileName(self):
        return self.text_with_json_filenames
    
    def isFileExists(self, file):
        if os.path.exists(file):
            return True
        else:
            return False
    
    def setListOfFiles(self):
        self.json_files = self.jsp.getListOfFiies(self.c_type)

    def getListOfFiles(self):
        return self.json_files
    
    def createTextFileWithJSONFiles(self):
        self.jsp.createTextFileWithJSONFiles(self.json_files, self.text_with_json_filenames)
        
    def getJSONFilesFromText(self):
        file = self.text_with_json_filenames
        res = []
        with open(file) as f:
            for line in f:
                filename = line.split(':')[0]
                if '\n' in filename:
                    filename = filename.rstrip('\n')
                    res.append(filename)
        return res
    
    
    def isClassnameToProcess(self,data):
        if data['_classname'] not in self.classnames_to_process:
            return False
        else:
            return True
        
    def isInterfaceToProcess(self,data):
        tut = data['_directly_provided']
        tat = self.interfaces_to_process
        res = set(tut).intersection(tat)
        if len(res) == 0:
            return False
        else:
            return True
    
    def getUniqueInterfaces(self):
        interfaces = []
        for i in self.getJSONFilesFromText():
            data = json.load(open(i))
            for k in data['_directly_provided']:
                interfaces.append(k)
        interfaces = self.getUniqueValuesList(interfaces)
        print(interfaces)   
        print(len(interfaces))
        
    def getUniqueValuesList(self, arr):
        a = set(arr)
        return sorted(list(a))
    