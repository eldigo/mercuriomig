import os, json, mmap

'''
Main class for database operaions 
'''        
class JSONProcessor():
    def __init__(self, json_folder):
        self.json_folder = json_folder
    '''
    Returns list of files contains "searchstring" in data['_path']
    Good for filtering single token as "hersteller" or "agentur" to furhter process  
    If no searchstring-parameter is provided the whole file list will be taken. For this case serachstring is defined as 'mercurio'. THis is the main folder in path and
    available in each file  
    '''
    def getListOfFiies(self, searchstring):
        print('Collecting files in the array....')
        json_files = []
        if searchstring == '':
            searchstring = 'mercurio'
        else:
            searchstring = '/'+searchstring+'/'
        for path, directories, filenames in os.walk(self.json_folder):
            for filename in filenames: 
                #print(os.path.join(path,filename))
                json_file = os.path.join(path,filename)
                try:
                    f = open(json_file)
                    data = json.load(f)
                    if searchstring in data['_path']:
                        #print(json_file)
                        json_files.append(json_file)
                except:
                    print('Corrupt: ', path)
                    '''corr_file = os.path.join(os.path.abspath('.\corruptedfiles'),filename)
                    print(corr_file)
                    os.rename(json_file, corr_file)'''
        return json_files

    def createTextFileWithJSONFiles(self, file_list, file_name):
        with open(file_name, 'w') as f:
            for line in file_list:
                f_string = line+'\n'
                f.write(f_string)
        return True
    
    def fulltextSearch(self, search_string):
        for path, directories, filenames in os.walk(self.json_folder):
            for filename in filenames: 
                #print(os.path.join(path,filename))
                json_file = os.path.join(path,filename)
                with open(json_file, 'rb', 0) as file, mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ) as s:
                    if s.find(b'Kundenliebling 2015') != -1:
                        print(json_file)