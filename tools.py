# -*- coding: UTF-8 -*-

import sys, getopt, time, configparser
from processor.JSONProcessor import JSONProcessor

def main(argv):
    sys.stdout = open(1, 'w', encoding='utf-8', closefd=False) # Fix stdout-problem with german umlaute.
    start_time = time.time() # Let's compute execution time.
    config = configparser.ConfigParser()
    config.read('./conf/main.ini')
    config.sections()
    main_config = config['MAIN'] # Get DB-configuratioins from config-file
    data_folder = main_config['data_folder']
    action = ''
    param = ''
    
    try:
       opts, args = getopt.getopt(argv,"ha:p:",["action=","parameter="])
    except getopt.GetoptError:
       sys.exit(2)
    for opt, arg in opts:
       print('++', opts, ' ', args) 
       if opt == '-h':
          print("%s -a <action> -p <parameter>" % sys.argv[0])
          sys.exit()
       elif opt in ("-a", "--action"):
          action = arg
       elif opt in ("-p", "--parameter"):
          parameter = arg
     
    if action == 'search':
       jp = JSONProcessor(data_folder)
       jp.fulltextSearch(parameter)
    
    
   #print('Action is "', action)
   #print('Parameter is "', parameter)

if __name__ == "__main__":
   main(sys.argv[1:])